codeunit 80200 "TCN_ImportacionPedidosVenta"
{
    Permissions = tabledata "Sales Header" = rmdi,
     tabledata "Sales Line" = rmdi;


    trigger OnRun()

    var

        plPedirDatos: Page TCN_PedirDatos;
        rlTempBlobTMP: Record TempBlob temporary;
        rlCustomer: Record customer;
        rlSalesLine: Record "Sales Line";
        rlSalesHeader: Record "Sales Header";
        xlInsStream: InStream;
        xlFichero: Text;
        xlLinea: text;
        rlTempCommentLine: Record "Comment Line" temporary;
        rlTempCommentLine2: Record "Comment Line" temporary;
        xlCodigoCliente: text[20];
        xlCodeCLiente: code[10];
        xlSeparador: text[1];
        xlModoCabecera: boolean;
        xlcampo: text;
        xlContadorLinea: Integer;
        xlCodigoCabecera: code[20];
        xlLineaPedido: Integer;
        xlPedidoImportado: label 'Importacion Realizada';
        xlCLienteNoEncontrado: label 'No existe el codigo de cliente';
        xlSeparadorVacio: label 'Separador Vacio';
    begin
        plPedirDatos.SetDatoF(xlCodigoCliente, 'Codigo Cliente');
        plPedirDatos.SetDatoF(xlSeparador, 'Separador');

        if plPedirDatos.RunModal() in [action::LookupOK, action::OK] then begin
            plPedirDatos.GetDatoF(xlCodigoCliente);
            plPedirDatos.GetDatoF(xlSeparador);
            xlModoCabecera := false;
            if Evaluate(xlCodeCLiente, xlCodigoCliente) then begin
                if rlCustomer.Get(xlCodeCLiente) then begin
                    if xlSeparador <> '' then begin


                        rlTempBlobTMP.Blob.CreateInStream(xlInsStream, TextEncoding::Windows);
                        if UploadIntoStream('Seleccion el fichero', '', 'Todos los archivos|*.*|Archivos de texto|*.txt;*.csv'
                                  , xlFichero, xlInsStream) then begin

                            while not xlInsStream.EOS do begin

                                xlInsStream.ReadText(xlLinea);
                                xlContadorLinea += 1;

                                xlCampo := CopyStr(xlLinea, 1, 1);
                                if UpperCase(xlCampo) = 'C' then begin
                                    xlModoCabecera := true;

                                    rlSalesHeader.Init();

                                    rlsalesHeader.validate("Document Type", rlSalesHeader."Document Type"::Order);



                                    if InsertarCabeceraF(xlCodeCLiente, rlSalesHeader, xlLinea, xlSeparador) then begin


                                        rlSalesHeader.validate("Sell-to Customer No.", xlCodeCLiente);

                                        rlSalesHeader.insert(true);
                                        xlCodigoCabecera := rlSalesHeader."No.";
                                        rlTempCommentLine2.Init();
                                        rlTempCommentLine2.Comment := CopyStr(StrSubstNo('Pedido %1 Insertado con Exito', xlCodigoCabecera), 1, MaxStrLen(rlTempCommentLine.Comment));
                                        rlTempCommentLine2."Line No." := xlContadorLinea;
                                        rlTempCommentLine2.Insert(false);
                                        Clear(rlSalesHeader);
                                        xlLineaPedido := 1;


                                    end else begin
                                        xlModoCabecera := false;
                                        rlTempCommentLine.Init();
                                        rlTempCommentLine.Comment := CopyStr(StrSubstNo('Lin %1, error %2', xlContadorLinea, GetLastErrorText), 1, MaxStrLen(rlTempCommentLine.Comment));
                                        rlTempCommentLine."Line No." := xlContadorLinea;
                                        rlTempCommentLine.Insert(false);

                                    end;


                                end else
                                    if UpperCase(xlCampo) = 'L' then begin
                                        if xlModoCabecera then begin
                                            rlSalesLine.init();
                                            if InsertarLineaPedidoF(xlCodigoCabecera, xlCodeCLiente, xlLineaPedido, rlSalesLine, xlLinea, xlSeparador) then begin
                                                rlSalesLine.Insert(true);
                                                Clear(rlSalesLine);
                                                xlLineaPedido += 100;


                                            end else begin
                                                rlTempCommentLine.Init();
                                                rlTempCommentLine.Comment := CopyStr(StrSubstNo('Lin %1, error %2', xlContadorLinea, GetLastErrorText), 1, MaxStrLen(rlTempCommentLine.Comment));
                                                rlTempCommentLine."Line No." := xlContadorLinea;
                                                rlTempCommentLine.Insert(false);
                                            end;
                                        end;

                                    end else begin
                                        rlTempCommentLine.Init();
                                        rlTempCommentLine.Comment := CopyStr(StrSubstNo('Error Linea %1 en el 1º Campo', xlContadorLinea), 1, MaxStrLen(rlTempCommentLine.Comment));
                                        rlTempCommentLine."Line No." := xlContadorLinea;
                                        rlTempCommentLine.insert(false);
                                    end;




                            end;

                            if rlTempCommentLine.count() = 0 then begin
                                message(xlPedidoImportado);

                            end else begin
                                page.Run(0, rlTempCommentLine);
                            end;

                            page.run(0, rlTempCommentLine2);

                        end;


                    end else begin
                        error(xlSeparadorVacio);
                    end;
                end else begin
                    Error(xlCLienteNoEncontrado);

                end;
            end;
        end;
    end;

    [TryFunction]
    local procedure InsertarCabeceraF(pCliente: code[20]; var pSalesHeader: Record "Sales Header"; pLinea: text; pSeparador: text)
    var

        xlCampo: Text;
        xlNumCampo: Integer;
    begin






        foreach xlCampo in pLinea.Split(pSeparador) do begin
            xlNumCampo += 1;

            case xlNumCampo of

                2:
                    begin
                        pSalesHeader.Validate("Order Date", ConvertirFechaF(xlCampo));
                    end;

                3:
                    begin
                        pSalesHeader.Validate("Document Date", ConvertirFechaF(xlCampo));
                    end;

                4:
                    begin
                        pSalesHeader.Validate("External Document No.", xlCampo);
                    end;

            end;


        end;
    end;

    [tryfunction]
    local procedure InsertarLineaPedidoF(pHeader: code[20]; pCliente: code[20]; pLineaPedido: Integer; var pSalesLine: Record "Sales Line"; pLinea: text; pSeparador: text)


    var
        xlCampo: Text;
        xlNumCampo: Integer;
        xlCantidad: decimal;
    begin

        pSalesLine.validate("Document Type", pSalesLine."Document Type"::Order);
        pSalesLine.validate("Document No.", pHeader);
        pSalesLine.Validate("Sell-to Customer No.", pCliente);
        pSalesLine.validate("Line No.", pLineaPedido);






        foreach xlCampo in pLinea.Split(pSeparador) do begin
            xlNumCampo += 1;

            case xlNumCampo of

                2:
                    begin

                        case UpperCase(xlCampo) of
                            'C':
                                begin
                                    pSalesLine.Validate(Type, pSalesLine.type::"G/L Account");
                                end;

                            'P':
                                begin
                                    pSalesLine.Validate(Type, pSalesLine.type::Item);
                                end;

                            else begin
                                    Error('Tipo de Linea');
                                end;
                        end;




                    end;

                3:
                    begin

                        pSalesLine.Validate("No.", xlCampo);
                    end;

                4:
                    begin
                        xlcampo := CopyStr(xlCampo, 1, MaxStrLen(pSalesLine.Description));
                        pSalesLine.Validate(Description, xlCampo);
                    end;

                5:
                    begin
                        Evaluate(xlCantidad, xlCampo);
                        xlCantidad /= 100;

                        pSalesLine.Quantity := xlCantidad //no hago el validate, no se puede en la funcion TRY
                    end;

                6:
                    begin
                        Evaluate(xlCantidad, xlCampo);
                        xlCantidad /= 100;
                        pSalesLine.Validate(pSalesLine."Unit Price", xlCantidad);
                    end;

                7:
                    begin
                        evaluate(pSalesLine."Line Discount %", xlCampo);
                        pSalesLine.Validate(pSalesLine."Line Discount %", pSalesLine."Line Discount %" / 100);
                    end;
            end;

        end;
    end;

    local procedure ConvertirFechaF(pFecha: text) xSalida: Date
    var
        xlDias: Integer;
        xlMes: integer;
        xlAno: integer;
    begin
        if pFecha <> '' then begin
            Evaluate(xlDias, CopyStr(pFecha, 1, 2));
            Evaluate(xlMes, CopyStr(pFecha, 3, 2));
            Evaluate(xlAno, CopyStr(pFecha, 5, 4));


            xSalida := DMY2Date(xlDias, xlMes, xlAno);
        end else begin
            xSalida := 0D;
        end;

    end;
}