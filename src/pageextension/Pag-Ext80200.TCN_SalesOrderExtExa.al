pageextension 80200 "TCN_SalesOrderExtExa" extends "Sales Order List" //MyTargetPageId
{
    layout
    {

    }

    actions
    {

        addlast(Navigation)
        {


            action(ImportacionDatos)
            {
                Caption = 'Ejercicio 2';
                Image = "8ball";

                trigger OnAction()

                var
                    culImportacionPedidosVenta: Codeunit TCN_ImportacionPedidosVenta;
                begin
                    culImportacionPedidosVenta.Run();
                end;
            }
            action(ContieneFiltro)
            {
                Caption = 'Ejercicio 3';
                Image = "8ball";

                trigger OnAction()
                begin
                    EstaElCodigoEnElFiltroF();
                end;
            }



        }
    }

    local procedure EstaElCodigoEnElFiltroF() xSalida: boolean

    //no funciona, tiene codigo de pruebas
    var
        plPaginaPedirDatos: page TCN_PedirDatos;
        xlCodigo: text[20];
        xlCode: code[20];
        xlFiltro: Text[80];
        xlfiltergroup: integer;
        rlTMPTablaEjercicio3: Record "Sales Header" temporary;
        xlFiltros: text;
        xlError: label 'No es un Codigo Valido';
        xlExito: label 'Lo contiene';
        xlNoExito: label 'No lo contiene';

    begin
        plPaginaPedirDatos.SetDatoF(xlCodigo, 'Codigo');
        plPaginaPedirDatos.SetDatoF(xlFiltro, 'Filtro');
        if plPaginaPedirDatos.RunModal() in [action::LookupOK, action::OK] then begin
            plPaginaPedirDatos.GetDatoF(xlCodigo);
            plPaginaPedirDatos.GetDatoF(xlFiltro);
        end;
        if Evaluate(xlCode, xlCodigo) then begin
            xlfiltros := GetFilters;
            rec.Reset();
            xlfiltergroup := FilterGroup();
            rlTMPTablaEjercicio3.FilterGroup(5);
            rlTMPTablaEjercicio3.Reset();
            rlTMPTablaEjercicio3.Init();
            rlTMPTablaEjercicio3."No." := xlCode;

            rlTMPTablaEjercicio3.Insert(false);

            rlTMPTablaEjercicio3.SetFilter("No.", xlFiltro);
            //rlTMPTablaEjercicio3.CopyFilters(Rec);
            if rlTMPTablaEjercicio3.Count = 0 then begin
                xSalida := false;
                Message(xlNoExito);

            end else begin
                xSalida := true;
                Message(xlExito);
            end;
        end else begin
            error(xlError);
        end;
        FilterGroup(xlfiltergroup);

    end;

}