page 80200 "TCN_PedirDatos"
{
    Caption = 'Importacion de Datos';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;


    layout
    {
        area(Content)
        {
            group(Datos)
            {
                field(field1; xTexto[1])
                {
                    CaptionClass = xArrayOfCaption[1];
                    Visible = xVisible1;

                }

                field(field2; xTexto[2])
                {
                    CaptionClass = xArrayOfCaption[2];
                    Visible = xVisible2;

                }

                field(field3; xDecimal[1])
                {
                    CaptionClass = xArrayOfCaption[3];
                    Visible = xVisible3;

                }

                field(field4; xDecimal[2])
                {
                    CaptionClass = xArrayOfCaption[4];
                    Visible = xVisible4;

                }

                field(field5; xdate[1])
                {
                    CaptionClass = xArrayOfCaption[5];
                    Visible = xVisible5;

                }

                field(field6; xdate[2])
                {
                    CaptionClass = xArrayOfCaption[6];
                    Visible = xVisible6;

                }





            }
        }
    }

    var
        xVisible1: Boolean;
        xVisible2: Boolean;
        xVisible3: Boolean;
        xVisible4: Boolean;
        xVisible5: Boolean;
        xVisible6: Boolean;

        xTexto: array[2] of text;
        xDecimal: array[2] of decimal;
        xdate: array[2] of date;


        xArrayOfCaption: array[6] of text[80];
        xArrayofContadoresSet: array[3] of decimal;
        xArrayofContadoresGet: array[3] of decimal;

        xCamposDeTexto: decimal;
        xCamposNumericos: decimal;
        xCamposDate: decimal;

    trigger OnInit()// cargo la plantilla de la pagina
    begin
        xCamposDeTexto := 2;
        xCamposNumericos := 2;
        xCamposDate := 2;
    end;

    local procedure CampoVisibleF(pCampo: integer; pCaption: text)

    var

    begin

        xArrayOfCaption[pCampo] := CopyStr('3,' + pCaption, 1, MaxStrLen(xArrayOfCaption[pCampo]));

        case pCampo of
            1:
                xVisible1 := true;

            2:
                xVisible2 := true;

            3:
                xVisible3 := true;

            4:
                xVisible4 := true;

            5:
                xVisible5 := true;

            6:
                xVisible6 := true;
        end;

    end;

    Procedure SetDatoF(pDato: text; pCaption: text)
    begin

        if xArrayofContadoresSet[1] < xCamposDeTexto then begin
            xArrayofContadoresSet[1] += 1;
            CampoVisibleF(xArrayofContadoresSet[1], pCaption);
        end;


    end;

    Procedure SetDatoF(pDato: decimal; pCaption: text)
    begin
        if xArrayofContadoresSet[2] < xCamposNumericos then begin
            xArrayofContadoresSet[2] += 1;
            CampoVisibleF(xArrayofContadoresSet[2] + xCamposDeTexto, pCaption);
        end;


    end;

    Procedure SetDatoF(pDato: date; pCaption: text)
    begin
        if xArrayofContadoresSet[3] < xCamposDeTexto then begin
            xArrayofContadoresSet[3] += 1;
            CampoVisibleF(xArrayofContadoresSet[3] + xCamposDeTexto + xCamposNumericos, pCaption);
        end;


    end;

    Procedure GetDatoF(var pDato: text) xSalida: boolean
    begin
        if xArrayofContadoresGet[1] < xCamposDeTexto then begin
            xArrayofContadoresGet[1] += 1;
            pDato := xTexto[xArrayofContadoresGet[1]];
        end;
    end;

    Procedure GetDatoF(var pDato: Decimal) xSalida: boolean
    begin
        if xArrayofContadoresGet[2] < xCamposNumericos then begin
            xArrayofContadoresGet[2] += 1;
            pDato := xDecimal[xArrayofContadoresGet[2]];
        end;
    end;

    Procedure GetDatoF(var pDato: date) xSalida: boolean

    begin
        if xArrayofContadoresGet[3] < xCamposDate then begin
            xArrayofContadoresGet[3] += 1;
            pDato := xdate[xArrayofContadoresGet[3]];
        end;
    end;








}